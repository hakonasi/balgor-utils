var canvas = document.getElementById('mycanvas');
var width = canvas.width;
var height = canvas.height;

var centerX = width/2;
var centerY = height/2;


var ctx = canvas.getContext("2d");

ctx.font = "18px serif";

const toDeg = 180/Math.PI;

ctx.lineWidth = 2;

canvas.addEventListener("mousemove", function(e) { 
    var cRect = canvas.getBoundingClientRect();        // Gets CSS pos, and width/height
    var canvasX = Math.round(e.clientX - cRect.left);  // Subtract the 'left' of the canvas 
    var canvasY = Math.round(e.clientY - cRect.top);   // from the X/Y positions to make  
    ctx.clearRect(0, 0, width, height);  // (0,0) the top left of the canvas

    var dx = canvasX - centerX;
    var dy = canvasY - centerY;
    var atanRes = Math.atan2(dx, dy);
    if(atanRes < 0){
        atanRes+=Math.PI*2;
    }
    ctx.fillText("atan2 (модифікований): " + atanRes.toFixed(3), 10, 30);
    
    ctx.fillText((atanRes * toDeg).toFixed(2) + "°", 10, 60);
    ctx.fillText("dx: " + dx + " dy: " + dy, 10, height-20);

    ctx.strokeStyle = '#000000';
    ctx.beginPath();
    ctx.moveTo(centerX, centerY);
    ctx.lineTo(canvasX, canvasY);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(centerX, centerY, 2, 0, 2 * Math.PI, false);
    ctx.fill();
    ctx.lineWidth = 5;
    ctx.strokeStyle = '#FF0000';
    ctx.stroke();
});